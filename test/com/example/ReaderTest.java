package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ReaderTest {

    @Mock
    Occurance occurance;

    @InjectMocks
    Reader reader;

    @Test
    public void readTest() throws IOException {
        List<String> text = Files.lines(Paths.get("Alchemist.txt"), StandardCharsets.UTF_8).collect(Collectors.toList());
    }

    @Test (expected = NoSuchFileException.class)
    public void readIncorrectPathExceptionTest() throws IOException {
        List<String> text = Files.lines(Paths.get("Alt.txt"), StandardCharsets.UTF_8).collect(Collectors.toList());
    }

    @Test (expected = UncheckedIOException.class)
    public void readIncorrectCharsetsExceptionTest() throws IOException {
        List<String> text = Files.lines(Paths.get("Alchemist.txt"), StandardCharsets.UTF_16).collect(Collectors.toList());
    }

    @Test
    public void splitTest() throws IOException {
        reader.split();
        assertEquals(reader.split().get(0), "Одиннадцать");
    }

    @Test
    public void replaceSignsTest() throws IOException {
        reader.replaceSigns(reader.split());
        assertFalse(reader.replaceSigns(reader.split()).get(1).contains("."));
        assertFalse(reader.replaceSigns(reader.split()).get(450).contains("-"));
    }

    @Test
    public void occuranseTest() throws IOException {
        reader.occuranse(reader.replaceSigns(reader.split()));
        assertEquals(reader.occuranse(reader.replaceSigns(reader.split())).get(1).getString(), "лет");
        assertFalse(reader.occuranse(reader.replaceSigns(reader.split())).get(1).getString()
                .equals(reader.occuranse(reader.replaceSigns(reader.split())).get(4).getString()));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void occuranseNegativeTest() throws IOException {
        reader.occuranse(reader.replaceSigns(reader.split()));
        assertTrue(reader.occuranse(reader.replaceSigns(reader.split())).get(2000).getString().equals(true)); // !!?
    }

    @Test
    public void sortTest() throws IOException {
        reader.sort(reader.replaceSigns(reader.split()));
        assertEquals(reader.sort(reader.replaceSigns(reader.split())).get(9), "");
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void sortNegativeTest() throws IOException {
        reader.sort(reader.replaceSigns(reader.split()));
        assertEquals(reader.sort(reader.replaceSigns(reader.split())).get(3333), ""); // ?
    }

    @Test
    public void remove() throws Exception {
        reader.remove(reader.replaceSigns(reader.split()));
        assertNotEquals(reader.remove(reader.replaceSigns(reader.split())).get(52), "где");
    }
}
