package com.example;

public class Occurance {
    private String string;
    private int count;
    public Occurance(String string, int count){
        this.string = string;
        this.count = count;
    }
    public String getString(){
        return string;
    }

    public int getCount(){
        return count;
    }
}
