package com.example;

import java.io.IOException;
import java.util.List;

public interface Tools {
    List<String> read() throws IOException;
    List<String> split() throws IOException;
    List<String> replaceSigns(List<String> list) throws IOException;
    List<Occurance> occuranse(List<String> list) throws IOException;
    List<String> sort(List<String> list) throws IOException;
    List<String> remove(List<String> list) throws IOException;
}
