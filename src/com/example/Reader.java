package com.example;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.*;

public class Reader implements Tools{
    private String[] spl;

    @Override
    public List<String> read() throws IOException {
        return Files.lines(Paths.get("Alchemist.txt"), StandardCharsets.UTF_8).collect(Collectors.toList());
    }

    @Override
    public List<String> split() throws IOException {
        List<String> splitter = new ArrayList<>();
        read().forEach( element -> splitter.addAll(Arrays.asList(spl = element.split(" "))));
        return splitter;
    }

    @Override
    public List<String> replaceSigns(List<String> list) throws IOException {
        List<String> clearSplit = new ArrayList<>();
        list.forEach(element -> clearSplit.addAll(Arrays.asList(element
                .replace("-", "")
                .replace(".", "")
                .replace(",", "")
                .replace("\"", "")
                .replace(":", "")
                .replace("?", "")
                .replace("!", "")
                .replace(";", "")
                .replace("(", "")
                .replace("(", "")
                .toLowerCase())));
        return clearSplit;
    }

    @Override
    public List<Occurance> occuranse(List<String> list) throws IOException {
        List<Occurance> occuranceList = new ArrayList<>();
        //list.stream().map(element -> new Occurance(element, frequency(occuranceList, element))).distinct().collect(Collectors.toList());
        for (int i = 0; i < list.size(); i++) {
                    Occurance occurance = new Occurance(list.get(i), frequency(list, list.get(i)));
                    occuranceList.add(occurance);
        }
        for (int i = 0; i < occuranceList.size(); i++) {
            for (int j = i+1; j < occuranceList.size(); j++) {
                if (occuranceList.get(i).getString().equals(occuranceList.get(j).getString())){
                    occuranceList.remove(j);
                }
            }
        }
        return occuranceList;
    }

    @Override
    public List<String> sort(List<String> list) throws IOException {
        return list.stream().sorted((o1, o2) -> o1.toString().compareTo(o2.toString())).collect(Collectors.toList());
    }

    @Override
    public List<String> remove(List<String> list) throws IOException {
        return list.stream().filter(element -> element.length()>3).collect(Collectors.toList());
    }
}