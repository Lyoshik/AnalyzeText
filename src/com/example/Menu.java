package com.example;

import java.io.IOException;

public class Menu {

    Reader reader = new Reader();
    public void act(String x) throws IOException {
        switch (x){
            case "1":
                reader.occuranse(reader.replaceSigns(reader.split())).forEach(element ->
                        System.out.println("Word \""+ element.getString()+ "\" used by " +element.getCount()+ " times!"));
                break;
            case "2":
                reader.sort(reader.replaceSigns(reader.split())).forEach(element -> System.out.println(element));
                break;
            case "3":
                reader.remove(reader.replaceSigns(reader.split())).forEach(element -> System.out.println(element));
                break;
            default:
                System.out.println("Press correct number!");
                break;
        }
    }
}
