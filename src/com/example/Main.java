package com.example;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Menu menu = new Menu();
        System.out.println("Hello! I have done some work with text file 'Alchemist.txt'.");
        System.out.println("All words is a single element in a list, all punctuation marks are removed.");
        System.out.println("And there is more operations, which you can see, if press correct number.");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Count occurance of same words in a list! (Press 1 for more:)");
        System.out.println("Words alphabetically sorted! (Press 2 for more:)");
        System.out.println("Words with lenght < = 3 removed! (Press 3 for more:)");
        String x = scanner.next();
        menu.act(x);
    }
}
